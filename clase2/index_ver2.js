"use strict"


window.onload= function(){
    console.log("se cargo la pagina")
    let divListado=document.getElementById("divListado");
    divListado.style.display="none";

    // asignacion de eventos
    document.calculos.btnCalcular.onclick=calcular;
    document.calculos.btnAgregar.onclick=()=>{
        divListado.style.display="block";

        let valor=document.getElementById("valor").innerText;

        let li = document.createElement("li");
        li.onclick = function(){
            console.log(this.innerText)
            // se busca al nodo padre (el ol)
            let ol=this.parentNode;
            // se elimina de la lista
            ol.removeChild(this);
           // debugger;
            if (ol.childNodes.length==0)
                divListado.style.display="none";
        }
        li.innerText = valor;
        
        let ol = document.getElementById("listado");
        ol.appendChild(li)

        document.calculos.precio.value=0;
        document.getElementById("resultado").innerHTML = ""
    };
    // no funciona con los radios button
    //document.calculos.iva.onclick=calcular;

    let ivas = document.getElementsByName("iva");
    for (let i=0;i<ivas.length;i++)
        ivas[i].addEventListener("click",calcular);

    // funcion arrow
    document.google.btnEnviar.onclick=()=>{
        document.google.btnEnviar.disabled=true
        document.google.submit()
    }
}

function calcular(){
    let f=document.calculos;

    let precio=parseFloat(f.precio.value);
    let iva=precio*f.iva.value;

    let resultado=precio+iva;

    document.getElementById("resultado").innerHTML=
        `El monto a pagar es <span id="valor">${resultado}</span>`;
    
}

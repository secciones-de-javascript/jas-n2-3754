
function validarCorreo(correo){
    let regEmail  = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
    
    return regEmail.test(correo);
}

function validarNumero(event,expReg){
    // si NO recibe por parametro expReg, se asume
    // que son numeros del 0 al 9
    if (expReg == undefined)
        expReg =/[0-9]/;
    // si presion backspace, se recibe
    if (event.keyCode == 8)
        return true;
    else{
        let letra = event.key;
        return  expReg.test(letra)
    }
}

let timeOut1, timeOut2;

function establecerTimeout(){
    document.getElementById("aviso").innerText= "";

    if (timeOut1 != undefined){
        clearTimeout(timeOut1);
    }
    if (timeOut2 != undefined){
        clearTimeout(timeOut2);
        timeOut2 = undefined;
    }

    timeOut1 = setTimeout(()=>{
        document.getElementById("aviso").innerText="Se te esta agotando el tiempo"
        timeOut2 = setTimeout(()=>{
            window.open("https://cadif1.com")
        },5000)
    },10000)
}

window.onload = ()=>{

    //establecerTimeout();
    let n=1;

    setInterval(()=>{
        if (n==1){
            document.title="hola";
            n=2;
        }else{
            document.title="chao";
            n=1;
        }
    },1000)

    window.onkeydown = establecerTimeout;
    window.onclick = establecerTimeout;

    datos.cedula.onblur = ()=>{

        if (datos.cedula.value.length == 0)
            datos.cedula.style.backgroundColor="red"
        else
            datos.cedula.style.backgroundColor="white"
    }

    datos.cedula.onkeydown = (event)=>{
        console.log("keydown: "+datos.cedula.value)
        if (datos.cedula.value.length == 0)
            return validarNumero(event,/[1-9]/);
        else
            return validarNumero(event);
    };
    datos.cedula.onkeypress = (event)=>{
        console.log("keypress: "+datos.cedula.value)
    };
    datos.cedula.onkeyup = (event)=>{
        console.log("keyup: "+datos.cedula.value)
        if (datos.cedula.value.length < 5)
            datos.correo.value= "muy corta"
        else
            datos.correo.value= "bien"
    };

    document.getElementById("btnGuardar").onclick=()=>{

        let regCed = /[0-9]/;
       
        if (!regCed.test(datos.cedula.value))
            alert("la cedula debe tener solo numeros")
        else
            if (!validarCorreo(datos.correo.value))
                alert("formato invalido del correo")
            else
                if (datos.ciudad.value == 0)
                    alert("Seleccione la ciudad");
                else
                    if (!datos.deacuerdo.checked)
                        alert("Debe estar de acuerdo")
                    else{
                        datos.correo.value=""
                        datos.cedula.value=""
                        datos.ciudad.selectedIndex = -1
                        datos.sexo[0].checked=false
                        datos.sexo[1].checked=false
                        datos.deacuerdo.checked=false
                    }
    }

}

let tablaAlumnos;

window.onload=()=>{
    tablaAlumnos = document.getElementById("tabla-alumnos"); 
    document.getElementById("btn-agregar").onclick = ()=>{
         //document.getElementById("formdatos").submit()
        if (validarDatos())
            if (buscarAlumno(formdatos.cedula.value.trim(),
                             tablaAlumnos))
                alert("Ya esta registrado")
            else{
                agregarFila(formdatos.cedula.value,formdatos.nombre.value);
                formdatos.cedula.value=""
                formdatos.nombre.value=""
            }
    }

    formdatos.onsubmit = (event)=>{
        console.log("se va a enviar")
        console.log(event)
        if (formdatos.cedula.value.length < 5){
            alert("Cedula invalida");
            // detiene la ejecucion normal del submit
            //event.preventDefault();
            return false;
        }
    }
}

function buscarAlumno(cedula,tabla){
    for (let i=1;i<tabla.rows.length;i++)
        if (cedula == tabla.rows[i].cells[1].innerText)
            return true;

    return false;
}

function validarDatos(){
    // debugger
     if (isNaN(formdatos.cedula.value) ||
         formdatos.cedula.value.length < 5){
         alert("Cedula invalida");
         return false
     }else
         if (formdatos.nombre.value.length < 3){
             alert("Nombre invalido");
             return false;
         }else
             return true;
 }

function agregarFila(cedula,nombre){
    let nuevaFila = tablaAlumnos.insertRow(-1)
    for (let i=0;i<5;i++){
        let nuevaCelda = nuevaFila.insertCell(-1)
        nuevaCelda.innerHTML = "&nbsp;"
    }

    let boton = document.createElement("input");
    boton.type = "button"
    boton.value = "Eliminar"
    boton.onclick = ()=>{
        tablaAlumnos.deleteRow(nuevaFila.rowIndex)
    }
    nuevaFila.cells[0].innerText = nuevaFila.rowIndex;
    nuevaFila.cells[1].innerText = cedula;
    nuevaFila.cells[2].innerText = nombre;
    nuevaFila.cells[nuevaFila.cells.length-1].appendChild(boton)
    
}


let titulo;
let monto;

function imprimir(){
    // se esconden elementos
    // que no queremos que se
    // impriman
    titulo.innerHTML += "Imprimiendo<br>"
    window.print()
    
}

function crearOtra(){
    let win = window.open("nueva.html","","width=600");
    win.document.open()
    
    win.document.write("<h1>Hola mundo</h1>")
    win.document.write("<p>afsdjfasdlflasdf as</p>")
    win.document.write("<img src='https://cadif1.com/img/template/logo-cadif1.svg' />")
    win.document.close()
    win.document.bgColor="yellow"
    win.document.title="Pagina escrita con JS"
    titulo.innerHTML += "Creando otra pagina<br>"
}

function abrirOtra(){
    titulo.innerHTML += "Abriendo otra ventana<br>"
    let win=window.open("otra.html","","width=600");
    win.moveTo(500,500)
}

function abrirGoogle(){
    titulo.innerHTML += "Abriendo google<br>"
    window.open("http://google.com","google",
                "left=500,width=650,height=850")
}

function goTo(){
    let url=prompt("Direccion:")
    window.location=url
}

function showSO(){
    titulo.innerHTML+=`El SO usado es ${window.navigator.platform} `+
                `esta en el idioma ${window.navigator.language}<br>`
}

function sumar(){
    let x = parseFloat(monto.innerText);
    x+=Math.random()*1000;
    monto.innerText =x.toFixed(2)
    document.title=x.toFixed(2)
}

function showScreenSizes(){
    titulo = document.getElementById("titulo");
    monto = document.getElementById("monto");

    monto.innerText = (Math.random()*1000).toFixed(2);

    console.clear()
    console.log(`El ancho de la pantalla es: ${window.screen.availWidth}`)
    console.log(`El alto de la pantalla es: ${window.screen.height}`)
    console.log(`El alto disponible de la pantalla es: ${window.screen.availHeight}`)
}

function showSizes(){
    titulo.innerHTML += "Mostrando medidas de la ventana<br>"
    showScreenSizes()
    console.log(`El ancho de la ventana es: ${window.innerWidth}`)
    console.log(`El alto de la ventana es: ${window.innerHeight}`)
}

function showScroll(){
    console.log(window.screenY)
}